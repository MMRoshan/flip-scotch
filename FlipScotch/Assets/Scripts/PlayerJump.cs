﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    private ParabolaController m_playerParabola;
    public RaycastGroundDetect m_raycastGroundDetect;


    //protected float m_parabolicJump;

    // Start is called before the first frame update
    void Start()
    {
        m_playerParabola = GetComponent<ParabolaController>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            if (m_playerParabola.Animation == false)
            {
                m_playerParabola.FollowParabola();
            }

            else
            {
                m_playerParabola.StopFollow();
                m_raycastGroundDetect.SnapToGround();

            }

        }


    }
}
