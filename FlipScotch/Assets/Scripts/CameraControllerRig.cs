﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CameraControllerRig : MonoBehaviour
{
    public float dampTime = 0.2f;
    public float screenEdgeBuffer = 4f;
    public float minZoomSize = 6.5f;
    /*[HideInInspector]*/ public Transform[] targets; //TODO: automatically add new paintcubes that are hit by player to a list

    private Camera m_Camera;
    private float zoomSpeed;
    private Vector3 moveVelocity;
    private Vector3 desiredPosition;

    private float camWidth;

    [SerializeField] private float camZoomLevel = 1f;
    [SerializeField] private float playerStartCamOffsetY;
    [SerializeField] private float playerStartCamOffsetZ;
    [SerializeField] private float playerExplodeCamOffsetY;
    [SerializeField] private float playerExplodeCamOffsetZ;

    [SerializeField] private float camZoomExplodeDuration = 5f;
    private float camZoomTimer = 0f;
    //[SerializeField] private GameManager gameManagerObject;
    private bool isCamExplodeZooming = false;


    public List<GameObject> m_targets;






    private void Awake()
    {


        m_Camera = GetComponentInChildren<Camera>();

        camWidth = m_Camera.pixelWidth;
    }

   
    


    private void LateUpdate() //TODO: if paintcubes not moving in fixedupdate, then change this to match their method
    {
        
        
        Move();
        Zoom();
        
    }








    private void ZoomAfterExplode() //TODO: Use this method for victory cam mvt
    {

        if(isCamExplodeZooming == true)
        {
            camZoomTimer += Time.fixedDeltaTime;
        }

        if (camZoomTimer <= camZoomExplodeDuration)
        {
            desiredPosition += (Vector3.up * playerExplodeCamOffsetY);
            desiredPosition += (Vector3.forward * -playerExplodeCamOffsetZ);
        }
        
    }







    private void Move()
    {
        FindAveragePosition();
        //FindAveragePosInList();
        //once desired position is found, smooth the cam to that position
        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref moveVelocity, dampTime);


    }



    /*
    private void FindAveragePosInList()
    {

        Vector3 averagePosInList = new Vector3();
        int numTargets = 0;

        for(int i = 0; i < m_targets.Count; i++)
        {
            if (!m_targets[i].gameObject.activeSelf)
                continue;

            averagePosInList += m_targets[i].transform.position;
            numTargets++;
        }

        if(numTargets > 0)
        {
            averagePosInList /= numTargets;
        }


        desiredPosition = averagePosInList;
    }*/


    #region FindAveragePosition() for Array, not list
    
    private void FindAveragePosition()
    {
        Vector3 averagePos = new Vector3();
        int numTargets = 0;

        for(int i = 0; i < targets.Length; i++) //TODO: make sure targets.length is set to number of paint cubes spawned. Do this in game manager script
        {
            //only cycle through active paintCubes
            if (!targets[i].gameObject.activeSelf) //&& IF TARGET ISLAUNCHED TRIGGER IS NOT ACTIVE (TARGET NOT HIT BY FLIPPER)
                continue; //if target not active continue to the next iteration of the loop

            averagePos += targets[i].position; //take active paintcube's position and add to avg pos
            numTargets++;
        }

        if(numTargets > 0)
        {
            averagePos /= numTargets;
        }

        //averagePos.y = transform.position.y; //TODO: adapt this to which axes are locked for paintcube. 

        desiredPosition = (averagePos + (Vector3.up*playerStartCamOffsetY) - (Vector3.forward*playerStartCamOffsetZ));

    }
    #endregion







    private void Zoom()
    {
        float requiredSize = FindRequiredSize();

        //zoom to the above required size
        camWidth = Mathf.SmoothDamp(m_Camera.pixelWidth, requiredSize, ref zoomSpeed, dampTime);  //TODO: camera.orthographic size used. Converted to aspect for perspective?
    }




    




    private float FindRequiredSize()
    {
        //find req size based on desired pos, not current pos
        Vector3 desiredLocalPos = transform.InverseTransformPoint(desiredPosition);

        float size = 0f;

        //zoom to accommodate the target that's furthest away
        for(int i = 0; i < m_targets.Count; i++)
        {
            if (!m_targets[i].gameObject.activeSelf)
                continue;

            Vector3 targetLocalPos = transform.InverseTransformPoint(m_targets[i].transform.position);

            Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

            //find the biggest distance 
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));

            //of the above biggest distance, check if it's bigger than the y value of the camera. (x is divided away with the aspect)
            size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / m_Camera.aspect);

        }
        //size is now the furthest value
        size += screenEdgeBuffer; //add a bit of extra distance TODO: adjust this

        //Make sure size is not too zoomed in
        size = Mathf.Max(size, minZoomSize);

        return size;
    }









    //Use this for game manager. When new level is loaded, snap camera to the start pos instead of smoothing to it
    public void SetStartPositionAndSize()
    {
        FindAveragePosition();

        transform.position = desiredPosition;

        m_Camera.aspect = FindRequiredSize();

        transform.position = this.transform.position + (Vector3.up * playerStartCamOffsetY) - (Vector3.forward * playerStartCamOffsetZ);
    }
}
