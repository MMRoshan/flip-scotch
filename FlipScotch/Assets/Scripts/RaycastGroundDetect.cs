﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastGroundDetect : MonoBehaviour
{
    private RaycastHit hit;
    private Vector3 groundContactpoint;
    [SerializeField] private float dropSpeed;
    private Rigidbody m_playerRb;
    public GameObject landIndicator;

    private void Awake()
    {
        m_playerRb = GetComponentInParent<Rigidbody>();
    }

    private void Update()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 100f))
        {
            Debug.DrawRay(transform.position, Vector3.down * hit.distance, Color.yellow);
            Debug.Log(hit.collider.gameObject);
            groundContactpoint = hit.point;
            landIndicator.transform.position = groundContactpoint + (transform.up * 0.2f) ;

        }

        if (Input.GetMouseButtonDown(0))
        {
        }
    }



    public void SnapToGround()
    {

        //transform.position = Vector3.MoveTowards(transform.position, groundContactpoint, Time.deltaTime / dropSpeed);
        //m_playerRb.position = Vector3.Lerp(transform.position, groundContactpoint, 1);
        m_playerRb.AddForce(-transform.up * 50, ForceMode.Impulse);
    }
    
}
